package HomeworkAndExamAVG;

public class Student {
	private String name;
	private double[] score;
	private double [] examscore;

	public Student(String name, double s1, double s2, double s3, double s4,
			double s5) {
		this.setName(name);
		score = new double[5];
		score[0] = s1;
		score[1] = s2;
		score[2] = s3;
		score[3] = s4;
		score[4] = s5;
	}
	public Student(String name , double s1 , double s2){
		this.name = name;
		examscore = new double[2];
		examscore[0] = s1;
		examscore[1]  = s2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getavg() {
		double avg = 0;
		for (int i = 0; i < score.length; i++) {
			avg = avg + score[i];
		}
		avg = avg / score.length;

		return avg;
	}
	public double getExamscoreAVG(){
		double avg = 0;
		for (int i = 0; i < examscore.length; i++) {
			avg = avg + examscore[i];
		}
		avg = avg /examscore.length;

		return avg;
	}

}
